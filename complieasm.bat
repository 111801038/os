nasm Sector1\bootloader.asm -f bin -o bootloader.bin
nasm Sector2\ExtendedSector.asm -f elf64 -o ExtendedSector.o

wsl $WSLENV/x86_64-elf-gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "Kernel.cpp" -o "Kernel.o"

wsl $WSLENV/x86_64-elf-ld -T"link.ld"

copy /b bootloader.bin+kernel.bin bootloader.flp

pause


